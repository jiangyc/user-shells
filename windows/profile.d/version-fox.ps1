
$vfoxAvailable = (Get-Command -Name vfox -ErrorAction SilentlyContinue) -ne $null

if ($vfoxAvailable) {
    Invoke-Expression "$(vfox activate pwsh)"
}

