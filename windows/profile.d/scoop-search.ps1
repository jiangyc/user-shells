
$scoopAvailable = (Get-Command -Name scoop -ErrorAction SilentlyContinue) -ne $null
$scoopSearchAvailable = (Get-Command -Name scoop-search -ErrorAction SilentlyContinue) -ne $null

if ($scoopAvailable -and $scoopSearchAvailable) {
    Invoke-Expression (&scoop-search --hook)
}

