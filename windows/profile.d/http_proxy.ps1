$proxy_ip         = $env:HTTP_PROXY_IP
if ([string]::IsNullOrEmpty($proxy_ip))
{
    $proxy_ip     = "127.0.0.1"
}
$proxy_port       = $env:HTTP_PROXY_PORT
if ([string]::IsNullOrEmpty($proxy_port))
{
    $proxy_port   = 11223
}
$proxy_url        = $proxy_ip + ":" + $proxy_port

# Proxy
function Proxy-Enable
{
    # terminal
    if ((-not $args[0]) -or ($args[0] -eq "terminal") -or ($args[0] -eq "term")) 
    {
        $env:HTTP_PROXY = "http://$proxy_url"; $env:HTTPS_PROXY = "http://$proxy_url"
	Proxy-Status terminal
    }
    # git
    if (Get-Command -Name "git" -ErrorAction SilentlyContinue)
    {
        if ((-not $args[0]) -or ($args[0] -eq "git")) 
        {
	    git config --global http.proxy "http://$proxy_url"; git config --global https.proxy "http://$proxy_url"
	    Proxy-Status git
	}
    }
    # scoop
    if (Get-Command -Name "scoop" -ErrorAction SilentlyContinue)
    {
	if ((-not $args[0]) -or ($args[0] -eq "scoop")) 
	{
	    scoop config proxy $proxy_url
	    Proxy-Status scoop
	}
    }
}

function Proxy-Status
{
    # terminal
    if ((-not $args[0]) -or ($args[0] -eq "terminal") -or ($args[0] -eq "term"))
    {
        echo "`nterminal:`n  http_proxy: $env:HTTP_PROXY`n  https_proxy: $env:HTTPS_PROXY"
    }
    # git
    if (Get-Command -Name "git" -ErrorAction SilentlyContinue)
    {
	if ((-not $args[0]) -or ($args[0] -eq "git"))
	{
            echo "`ngit:`n  http.proxy: $(git config --global http.proxy)`n  https.proxy: $(git config --global https.proxy)"
	}
    }
    # scoop
    if (Get-Command -Name "scoop" -ErrorAction SilentlyContinue)
    {
	if ((-not $args[0]) -or ($args[0] -eq "scoop")) 
	{
            echo "`nscoop:`n  proxy: $(scoop config proxy)"
	}
    }
}

function Proxy-Disable
{
    # terminal
    if ((-not $args[0]) -or ($args[0] -eq "terminal") -or ($args[0] -eq "term")) 
    {
	$env:HTTP_PROXY=""; $env:HTTPS_PROXY=""
	Proxy-Status terminal
    }
    # git
    if (Get-Command -Name "git" -ErrorAction SilentlyContinue)
    {
	if ((-not $args[0]) -or ($args[0] -eq "git"))
	{
            git config --global --unset http.proxy; git config --global --unset https.proxy
	    Proxy-Status git
	}
    }
    # scoop
    if (Get-Command -Name "scoop" -ErrorAction SilentlyContinue)
    {
	if ((-not $args[0]) -or ($args[0] -eq "scoop")) 
        {
	    scoop config rm proxy
            Proxy-Status scoop
	}
    }
}
