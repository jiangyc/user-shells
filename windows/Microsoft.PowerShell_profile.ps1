# ========== $PROFILE ==========

# 加载扩展脚本（默认是脚本所在目录下的profile.d目录）
$ps_root_dir    = Split-Path $MyInvocation.MyCommand.Path
$ps_ext_dir     = "$ps_root_dir\profile.d"
if (Test-Path -Path $ps_ext_dir -PathType Container)
{
    Get-ChildItem -Path $ps_ext_dir -Filter *.ps1 | Where-Object { $_.FullName -ne $ps_ext_dir } | ForEach-Object { . $_.FullName }
}

