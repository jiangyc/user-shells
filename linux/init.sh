# scripts dir
_user_script_path=$(readlink -f "$0")
_user_script_path=$(dirname "$_user_script_path")

_load_user_scripts() {
    local directory="$1"  

    if [ -z "$1" ]; then
        echo "script's dir can not be empty."
        return 0
    fi
    
    if [ -d "$directory" ]; then
        if find "$directory" -type f -name "*.sh" -print -quit | grep -q .; then
            local sh_files=("$directory"/*.sh)

            for shell_file in "${sh_files[@]}"; do
                if [ -f "$shell_file" ]; then
                    # echo $shell_file
                    source $shell_file
                fi
            done
        fi
    fi
}

# base
_load_user_scripts "$_user_script_path/base"
# envs
_load_user_scripts "$_user_script_path/envs"

