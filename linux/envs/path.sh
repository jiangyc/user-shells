#!/bin/bash

# PATH: ~/bin
if [ -d "$HOME/bin" ]; then
	if [[ ! ":$PATH:" == *:"$HOME/bin":* ]]; then
		export PATH=$PATH:$HOME/bin
	fi
fi

# PATH: ~/.local/bin
if [ -d "$HOME/.local/bin" ]; then
	if [[ ! ":$PATH:" == *:"$HOME/.local/bin":* ]]; then
		export PATH=$PATH:$HOME/.local/bin
	fi
fi
