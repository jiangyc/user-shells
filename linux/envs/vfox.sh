#!/bin/bash

# ******************** VFOX ********************
export VFOX_HOME=$HOME/.version-fox
if [ -f "/usr/bin/vfox" ]; then
    eval "$(vfox activate bash)"
fi
